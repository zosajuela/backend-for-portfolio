const express = require('express');
const mongoose = require('mongoose');

const app = express()
const path = require('path')

const cors = require('cors')



app.use(cors())

require('dotenv').config()

const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connect(connectionString, {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})



app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// app.get('/', (req,res) => {
// 	res.send("successfully hosted")
// })

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open',()=> console.log("Now connected to MongoDB Atlas DataBase"));

app.get('/', function(req,res){
	res.send(`you are now connected on port ${port}`);
})

const userRoutes = require('./routes/user')

app.use('/api/users', userRoutes)

app.listen(port, ()=> console.log(`you got served on port ${port}`));