const mongoose = require('mongoose')

//we are now going to create the schema for our users
const userSchema = new mongoose.Schema({
     company: {
     	type: String
     },
     subject: {
     	type: String
     }, 
     email: {
     	type: String
     },
     message: {
     	type: String
     }
}) 

module.exports = mongoose.model('user', userSchema)